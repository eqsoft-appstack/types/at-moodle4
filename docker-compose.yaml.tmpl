version: '3.9'

services:
  {{ .App }}moodle:
    env_file:
      - .env
    image: {{ .Env.moodle_image }}
    container_name: {{ .AppId }}-moodle
    restart: always
    environment:
      - MOODLE_USERNAME=root
      - MOODLE_PASSWORD=root
      - MOODLE_HOST={{ .App }}.{{ .ExternalDomain }}
      - MOODLE_DATABASE_HOST={{ .App }}mariadb
      - MOODLE_DATABASE_PORT_NUMBER=3306
      - MOODLE_DATABASE_USER={{ .App }}
      - MOODLE_DATABASE_PASSWORD={{ .App }}
      - MOODLE_DATABASE_NAME={{ .App }}
      - APACHE_HTTPS_PORT_NUMBER=443
      - APACHE_HTTP_PORT_NUMBER=80
    labels:
      - "appstack.version=0.0"{{ if .Env.traefik }}
      - "traefik.docker.network={{ .Env.traefik_net }}"
      - "traefik.enable=true"
      - "traefik.http.routers.{{ .App }}moodle-secure.entrypoints=websecure"
      - "traefik.http.routers.{{ .App }}moodle-secure.middlewares=default@file"
      - "traefik.http.routers.{{ .App }}moodle-secure.rule=Host(`{{ .App }}.{{ .ExternalDomain }}`)"
      - "traefik.http.routers.{{ .App }}moodle-secure.service={{ .App }}moodle"
      - "traefik.http.routers.{{ .App }}moodle-secure.tls=true"
      - "traefik.http.routers.{{ .App }}moodle-secure.tls.certresolver=http_resolver"
      - "traefik.http.routers.{{ .App }}moodle.entrypoints=web"
      - "traefik.http.routers.{{ .App }}moodle.rule=Host(`{{ .App }}.{{ .ExternalDomain }}`)"
      - "traefik.http.services.{{ .App }}moodle.loadbalancer.server.port=80"{{ end }}
    volumes:
    - moodle:/bitnami/moodle
    - moodledata:/bitnami/moodledata
    - certs:/certs:z
    depends_on:
    - {{ .App }}mariadb
    networks:
      default:
        aliases:
         - "{{ .App }}.{{ .Domain }}"{{ if .Env.traefik }}
      {{ .Env.traefik_net }}:
        aliases:
          - "{{ .App }}.{{ .ExternalDomain }}"{{ end }}

  {{ .App }}mariadb:
    env_file:
      - .env
    image: {{ .Env.mariadb_image }}
    container_name: {{ .AppId }}-mariadb
    restart: always
    environment:
    - MARIADB_ROOT_PASSWORD=root
    - MARIADB_USER={{ .App }}
    - MARIADB_DATABASE={{ .App }}
    - MARIADB_PASSWORD={{ .App }}
    - MARIADB_CHARACTER_SET=utf8mb4
    # https://stackoverflow.com/questions/75122050/mariadb-on-docker-cannot-write-to-named-volume
    - MARIADB_EXTRA_FLAGS=--innodb_flush_method=fsync
    - MARIADB_COLLATE=utf8mb4_unicode_ci
    - TZ=Europe/Berlin
    volumes:
    - mariadb:/bitnami/mariadb

volumes:
  mariadb:
    name: "{{ .AppId }}-mariadb"
  moodle:
    name: "{{ .AppId }}-moodle"
  moodledata:
    name: "{{ .AppId }}-moodledata"
  certs:
    name: "as-global-certs-certs"

networks:
  default:
    name: "{{ .Net }}"
    external: true{{ if .Env.traefik }}
  {{ .Env.traefik_net }}:
    name: "{{ .Env.traefik_net }}"
    external: true{{ end }}

